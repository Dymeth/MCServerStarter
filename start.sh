#!/bin/sh

# All system paths and filenames are case sensitive (uppercase and lowercase letters)
# Все системные пути и названия файлов чувствительны к регистру (заглавные и строчные буквы)

# *** Server app options / Параметры приложения-сервера ***

# Execution type: screen or tmux
# Тип выполнения: screen или tmux
execution_type="screen"

# To create a screen/tmux session and identify the process in top, htop, etc.
# Для создания сессии screen/tmux и идентификации процесса в top, htop и т.д.
server_name="lobby"

# Display the server console after starting the script
# Отображать ли консоль сервера сразу после запуска скрипта
auto_console_join=true

# Server app file name. If not specified, it will automatically find the jar file. Example: paper.jar
# Название файла ядра. Если не указано - найдёт jar-файл автоматически. Пример: paper.jar
jar_file=""

# Minimum amount of memory. Example: 2048M or 2G
# Минимальное количество памяти. Пример: 2048M или 2G
min_memory="1G"

# Maximum amount of memory
# Максимальное количество памяти
max_memory="5G"

# Server port. If not specified, the value from server.properties is used
# Порт сервера. Если не указан - используется значение из server.properties
server_port=""

# Whether to use a forced update of all chunks to the format of the current Minecraft-server version at startup
# Использовать ли при запуске принудительное обновление всех чанков до формата текущей версии Майнкрафт-сервера
force_chunks_upgrade=false

# *** Java options / Параметры Java ***

# Java directory (for JDK add "/bin" at the end). If not specified, the system path is used. Example: $HOME/jre-16.0.1 или $PWD/jdk-17.0.2/bin
# Директория Java (для JDK добавьте в конце "/bin"). Если не указана - используется системный путь. Пример: $HOME/jre-16.0.1 или $PWD/jdk-17.0.2/bin
java_dir=""

# Whether to enable profiling (requires more system resources). Only works with JDK
# Включить ли профилирование (требуется больше системных ресурсов). Работает только с JDK
profiling=false

# Whether to prevent errors like <<module java.base does not "opens java.lang" to unnamed module>>
# Предотвращать ли ошибки вида <<module java.base does not "opens java.lang" to unnamed module>>
fix_java_12_issues=true

# The host and port of the debug agent. If not specified, debugging is disabled. Example: 0.0.0.0:22222
# Хост и порт агента отладки. Если не указано - отладка отключена. Пример: 0.0.0.0:22222
debug_agent_address=""

# Log4j options file with .xml extension. If not specified, default settings are used. Example: log4j2.xml
# Файл параметров log4j с расширением .xml. Если не указано - используются настройки по-умолчанию. Пример: log4j2.xml
log4j_config=""



# *** Anything below is not recommended to be modified ***
# *** Всё, что ниже, менять не рекомендуется ***

# Source / Источник: https://mcflags.emc.gs
aikar_jvm_flags="-XX:+UseG1GC -XX:+ParallelRefProcEnabled -XX:MaxGCPauseMillis=200 -XX:+UnlockExperimentalVMOptions -XX:+DisableExplicitGC -XX:+AlwaysPreTouch -XX:G1NewSizePercent=30 -XX:G1MaxNewSizePercent=40 -XX:G1HeapRegionSize=8M -XX:G1ReservePercent=20 -XX:G1HeapWastePercent=5 -XX:G1MixedGCCountTarget=4 -XX:InitiatingHeapOccupancyPercent=15 -XX:G1MixedGCLiveThresholdPercent=90 -XX:G1RSetUpdatingPauseTimePercent=5 -XX:SurvivorRatio=32 -XX:+PerfDisableSharedMem -XX:MaxTenuringThreshold=1 -Dusing.aikars.flags=https://mcflags.emc.gs -Daikars.new.flags=true"
jvm_flags="-server -Dfile.encoding=UTF-8"
app_flags="nogui"
display_start_command=false
process_hint="${server_name}"
script_name=$(basename "$0")

if [ "$jar_file" = "" ]; then
	for file in *.jar; do
		[ -f "$file" ] || break
		if [ "$jar_file" != "" ]; then
			echo "Multiple jar-files found: $jar_file and $file. Please specify jar_file option in $script_name"
			exit
		fi
		jar_file=$file
	done
	if [ "$jar_file" = "" ]; then
		echo "Jar-file not found. Please specify jar_file option in $script_name"
		exit
	fi
	echo "Using jar-file $jar_file"
fi

if [ "$1" != "deep" ]; then
	if [ "$execution_type" = "screen" ]; then
		screen -A -m -d -S ${server_name} bash "${script_name}" deep
		[ "$auto_console_join" = true ] && sleep 0.2 && screen -x ${server_name}
	elif [ "$execution_type" = "tmux" ]; then
		tmux new -d -s ${server_name}
		sleep 0.2
		tmux send-keys -t ${server_name} "bash \"${script_name}\" deep" Enter
		[ "$auto_console_join" = true ] && sleep 0.2 && tmux attach -t ${server_name}
	else
		echo "Wrong execution_type: $execution_type"
	fi
	exit
fi

#!/bin/bash

[ "$java_dir" != "" ] && java_dir="${java_dir}/"
java_binary="${java_dir}java"

jvm_flags+=" ${aikar_jvm_flags}"
if [ "$profiling" = true ]; then
	process_hint+="-profiling"
	jvm_flags="${jvm_flags// -XX:+PerfDisableSharedMem/}"
	jvm_flags+=" -Xshare:off"
fi
process_hint="$USER.${process_hint}"

add_opens_packages=()
add_modules_list=(
	"jdk.incubator.vector"
)

if [ "$fix_java_12_issues" = true ]; then
	add_opens_packages+=(
		"java.base/java.lang"
		"java.base/java.lang.reflect"
		"java.base/java.lang.invoke"
		"java.base/java.security"
	)
fi

for package in "${add_opens_packages[@]}"; do
	jvm_flags+=" --add-opens ${package}=ALL-UNNAMED"
done

for module_index in "${!add_modules_list[@]}"; do
	if [ "$module_index" = 0 ]; then
		jvm_flags+=" --add-modules="
	else
		jvm_flags+=","
	fi
	jvm_flags+="${add_modules_list[$module_index]}"
done

[ "$debug_agent_address" != "" ] && jvm_flags+=" -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=${debug_agent_address}"
[ "$log4j_config" != "" ] && jvm_flags+=" -Dlog4j.configurationFile=${log4j_config}"

[ "$server_port" != "" ] && app_flags+=" -port ${server_port}"
[ "$force_chunks_upgrade" = true ] && app_flags+=" --forceUpgrade"

start_command="${java_binary} -D_server=${process_hint} -Xms${min_memory} -Xmx${max_memory} ${jvm_flags} -jar ${jar_file} ${app_flags}"

if [ "$display_start_command" = true ]; then
	echo "Start command: $start_command"
fi

while true
do
	eval "${start_command}"
	echo "Server $server_name stopped. Rebooting in:"
	for seconds_left in {3..1}
	do
		echo "$seconds_left..."
		sleep 1
	done
done

# Author / Автор: dymeth.ru
